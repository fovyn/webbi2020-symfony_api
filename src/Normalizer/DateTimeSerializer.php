<?php


namespace App\Normalizer;


use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class DateTimeSerializer implements SerializerInterface
{

    /**
     * @inheritDoc
     * @param \DateTime $object
     */
    public function serialize($object, string $format = null, array $context = [])
    {
        return $object->format(\DateTime::ISO8601);
    }


    /**
     * @inheritDoc
     */
    public function deserialize($data, string $type, string $format, array $context = [])
    {
        return "";
    }
}