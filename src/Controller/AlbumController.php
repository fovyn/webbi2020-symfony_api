<?php

namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumCreateType;
use App\Model\Album\AlbumCreateForm;
use App\Repository\ArtistRepository;
use App\Repository\TrackRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AlbumController
 * @package App\Controller
 * @Rest\Route("/api/album")
 */
class AlbumController extends AbstractController
{
    /**
     * @Rest\Get("/", name="album_read-all")
     * @Rest\View()
     */
    public function readAllAction(ArtistRepository $repository) {
        return $repository->findAll();
    }

    /**
     * @Rest\Post("/", name="album_create")
     * @Rest\View()
     * @param Request $createForm
     * @param EntityManagerInterface $manager
     * @return mixed
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $createForm, EntityManagerInterface $manager) {
        $album = new AlbumCreateForm();
        $data = json_decode($createForm->getContent(), true);
        $formType = $this->createForm(AlbumCreateType::class, $album);
        $formType->submit($data);

        if ($formType->isValid()) {
            $newAlbum = new Album();
            $newAlbum->setTitle($album->getTitle());

            $manager->persist($newAlbum);
            $manager->flush();

            return ["success" => "Album créé"];
        }


        return ["error" => "Album non créé"];
    }
}
