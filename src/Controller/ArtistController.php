<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Entity\Track;
use App\Repository\ArtistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArtistController
 * @package App\Controller
 * @Route(path="/api/artist")
 */
class ArtistController extends AbstractController
{
    /**
     * @Route(path="/", name="artist_read-all", methods={"GET"})
     * @param ArtistRepository $artistRepository
     * @return JsonResponse
     */
    public function readAllAction(ArtistRepository $artistRepository) {
        return $this->json($artistRepository->findAll());
    }

    /**
     * @Route(path="/{name}", name="artist_read-one", methods={"GET"})
     * @param ArtistRepository $artistRepository
     * @param string $name
     * @return JsonResponse
     */
    public function readOneAction(ArtistRepository $artistRepository, string $name) {
        return $this->json($artistRepository->findOneBy(["name" => $name]));
    }

    /**
     * @Route(path="/{name}/tracks", name="artist_read-one-tracks", methods={"GET"})
     * @param string $name
     * @param ArtistRepository $artistRepository
     * @return JsonResponse
     */
    public function readTracksByAuthorAction(string $name, ArtistRepository $artistRepository) {
        $tracks = $artistRepository->findTracksByArtistName($name);

        return $this->json($tracks);
    }

    /**
     * @Route(path="/", name="artist_create", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     */
    public function createAction(Request $request, EntityManagerInterface $manager) {
        $content = $request->getContent();
        $artistCreateDto = json_decode($content, true);

        $artist = new Artist();
        $artist->setName($artistCreateDto["name"]);
        $artist->setNationality($artistCreateDto["nationality"]);


        $manager->persist($artist);
        $manager->flush();

        return $this->json([
            "success" => "Artiste créé"
        ], 203);
    }

    /**
     * @param Request $request
     * @param ArtistRepository $artistRepository
     * @param EntityManagerInterface $manager
     * @param string $name
     * @return JsonResponse
     * @Route(path="/{name}/tracks", name="artist_patch-track", methods={"PATCH"})
     */
    public function addTrackToArtist(Request $request, ArtistRepository $artistRepository, EntityManagerInterface $manager, string $name) {
        $artist = $artistRepository->findOneBy(["name" => $name]);

        if ($artist == null) {
            return $this->json(["error" => "Artist not found"], 404);
        }
        $content = json_decode($request->getContent(), true);

        $track = new Track();
        $track->setAuthor($artist);
        $track->setDuration($content["duration"]);
        $track->setTitle($content["title"]);

        $artist->addTrack($track);

        $manager->flush();

        return $this->json(["success" => "La piste a bien été ajoutée"], 200);
    }

    /**
     * @Route(path="/{name}", name="artist_delete", methods={"DELETE"})
     */
    public function deleteAction(string $name, ArtistRepository $artistRepository) {
        try {
            $artistRepository->deleteByName($name);
            return $this->json(["success" => "Artist supprime"], 200);
        } catch (\Exception $e) {
            return $this->json(["error" => "Artist non supprime"], 500);
        }
    }
}
