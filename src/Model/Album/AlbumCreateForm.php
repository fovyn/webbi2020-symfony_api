<?php


namespace App\Model\Album;


use Symfony\Component\Validator\Constraints as Assert;

class AlbumCreateForm
{
    /**
     * @var string $title
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(min="5", minMessage="Le titre doit être de 5 caractères minimum", max="100")
     */
    private $title;

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return AlbumCreateForm
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }


}