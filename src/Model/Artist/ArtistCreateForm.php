<?php


namespace App\Model\Artist;


use Symfony\Component\Validator\Constraints as Assert;

class ArtistCreateForm
{
    /**
     * @var string $name
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $name;
    /**
     * @var string $nationality
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $nationality;
    /**
     * @var \DateTime $birthDate
     * @Assert\DateTime(format="yyyy-mm-dd")
     */
    private $birthDate;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNationality(): string
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality(string $nationality): void
    {
        $this->nationality = $nationality;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate(): \DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     */
    public function setBirthDate(\DateTime $birthDate): void
    {
        $this->birthDate = $birthDate;
    }


}