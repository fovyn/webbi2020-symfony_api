<?php

namespace App\Entity;

use App\Repository\TrackRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrackRepository::class)
 */
class Track implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="float")
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity=Artist::class, inversedBy="tracks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\ManyToMany(targetEntity=Artist::class)
     */
    private $interpret;

    public function __construct()
    {
        $this->interpret = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDuration(): ?float
    {
        return $this->duration;
    }

    public function setDuration(float $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getAuthor(): ?Artist
    {
        return $this->author;
    }

    public function setAuthor(?Artist $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getInterpret(): Collection
    {
        return $this->interpret;
    }

    public function addInterpret(Artist $interpret): self
    {
        if (!$this->interpret->contains($interpret)) {
            $this->interpret[] = $interpret;
        }

        return $this;
    }

    public function removeInterpret(Artist $interpret): self
    {
        if ($this->interpret->contains($interpret)) {
            $this->interpret->removeElement($interpret);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "title" => $this->getTitle(),
            "duration" => $this->getDuration()
        ];
    }
}
